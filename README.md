# dns consistency check

## Purpose

Check all dns entries in a given 24-subnet for consistency.
Performs the following tests:
1. IPv4 -> hostname
2. hostname -> IPv4

## Usage

dns-check-consistency.sh <local domain name> <ipv4 prefix>

for example:
dns-check-consistency.sh "mylan" "192.168.178"

The result is written to a file named that contains the local domain name: dns-consistency-mylan
This is what it looks like:

```
  1,    192.168.11.1,        router.mylan.,                                ,          is alive,      name syntax ok,       A matches PTR
  1,    192.168.11.2,      repeater.mylan.,                                ,          is alive,      name syntax ok,       A matches PTR
  1,    192.168.11.3,       printer.mylan.,                                ,    is unreachable,      name syntax ok,       A matches PTR
  1,    192.168.11.4,      doorbell.mylan.,       fe80::a65d:77ff:f226:4616,          is alive,      name syntax ok,       A matches PTR

```
## Authors and acknowledgment
Joachim Schwender joachim.schwender@web.de

## License

GNU License v2

## Project status
 started