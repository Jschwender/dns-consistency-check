#!/bin/bash
#
# checks all A and PTR entries of a given addres range of a DNS for consistency
#
DNS=$(dig 1.1.1.1|grep SERVER|cut -d'(' -f2|cut -d ')' -f1)  #'
LOCALDOMAIN=$1      # local top level domain
PREFIX=$2           # something like "192.168.1" for a 24-subnet
AUSGABE=dns-consistency-$LOCALDOMAIN
rm -f "$AUSGABE"

function validate_hostname()  {
# text string l <= 24
# chars allowed: [a-z,A-Z,0-9]  '-'  '.'
# '.' and '-' are not allowed on 1st place and not at the end
# 1st char must be [a-zA-Z]
# no distinction between upper and lower
#
# function returns nothing if name is validated, otherwise an error message
    LOWER=$(echo $1|sed -e 's/\(.*\)/\L\1/')  # ' all in lower case
    echo "$LOWER"|grep --quiet -e '-$' && { echo "$1		hyphen at the end not allowed"; return 1; }
    echo "$LOWER"|grep --quiet -e '^-' && { echo "$1		hyphen not allowed as 1st char"; return 1; }
    echo "$LOWER"|grep --quiet -e '^\.' && { echo "$1		dot not allowed as 1st char"; return 1; }
    echo "$LOWER"|grep --quiet -e '\.\.' && { echo "$1		double dots not allowed"; return 1; }
    echo "$LOWER"|grep --quiet -e "\.${LOCALDOMAIN}\.$" || { echo "$1		does not end with valid auth domain"; return 1; }
    REST=$(echo $LOWER|sed -e 's/[a-z0-9]//g' -e 's/\.//g' -e 's/-//g')
    [ -n "$REST" ] && { echo "$1		contains illegal chars"; return 1; }
    echo "name syntax ok"
    return 0
}

function validate_examples() {  # for testing only
validate_hostname _gateway.$LOCALDOMAIN
validate_hostname ga[teway.$LOCALDOMAIN
validate_hostname ga.teway.$LOCALDOMAIN
validate_hostname .teway.$LOCALDOMAIN
validate_hostname ga.teway.$LOCALDOMAIN
validate_hostname gate@Way.$LOCALDOMAIN
validate_hostname gate:Way.$LOCALDOMAIN
validate_hostname -ga.teway.$LOCALDOMAIN
validate_hostname ga..teway.$LOCALDOMAIN
validate_hostname gateway.fte.lcxatzu
validate_hostname ga-teway.$LOCALDOMAIN
validate_hostname gAteWaY.$LOCALDOMAIN
}

function ip2hostname() {  # verwendet DNS als globale Variable!
    dig @$DNS +short -x $1
}

function name2ip() {
    dig @$DNS +short -t A $1
}

function validate_DNS() {
    NHN=1
    ip2hostname $1|while read HN
    do
	VALIDHN=$(validate_hostname $HN)
	HOST_REACH=$(fping -4 --retry=1 --timeout=300 --ttl=3 $1|sed "s/$1//g")
	LINKLOCAL=$(echo $HN|sed "s/\.${LOCALDOMAIN}\./.local/g")
	echo $LINKLOCAL

	name2ip $HN|while read DNSA
	do
#		echo "$NHN : $IPA : $HN : $DNSA"
		if [ $1 != $DNSA ]
		then
			CONSISTANCY="A-PTR mismatch error $DNSA"
		else
			CONSISTANCY='A matches PTR'
		fi
		IPV6=""
		echo "$HOST_REACH" |grep --quiet "is alive" && IPV6=$(avahi-resolve -6 -n $LINKLOCAL|awk '{print $2}')
		#echo "$NHN : $1 : $HN : $IPV6 : $HOST_REACH : $VALIDHN : $CONSISTANCY"  >> ${AUSGABE}
		printf "%3s,%16s,%20s,%32s,%18s,%20s,%20s\n" "$NHN" "$1" "$HN" "$IPV6" "$HOST_REACH" "$VALIDHN" "$CONSISTANCY"  >> ${AUSGABE}
	done
	((NHN++))
    done
}

for i in $(seq 0 255)
do
    IPA=${PREFIX}.$i
    validate_DNS $IPA
done 


echo "total:		$(cat $AUSGABE|wc -l )" >> "$AUSGABE"
echo "A-PTR matches:		$(grep ' A matches PTR' $AUSGABE|wc -l )" >> "$AUSGABE"
echo "A-PTR mismatches:		$(grep 'mismatch error' $AUSGABE|wc -l )" >> "$AUSGABE"
echo "syntax ok:		$(grep 'syntax ok' $AUSGABE|wc -l )" >> "$AUSGABE"
echo "reachable:		$(grep ' is alive' $AUSGABE|wc -l )" >> "$AUSGABE"
echo "unreachable:		$(grep ' is unreachable' $AUSGABE|wc -l )" >> "$AUSGABE"
